import React, { useReducer, useState } from 'react'
import {
  Box,
  Button,
  Card,
  CardContent,
  Checkbox,
  FormControlLabel,
  Grid,
  IconButton,
  Paper,
  TextField,
  Typography
} from '@mui/material'
import Page from '../components/Page'
import Tabs from '../components/Tabs'
import Icon from 'src/components/Icon'
import DataTable from 'src/components/DataTable'
import { validateSchema } from './schema'
import { get, keys, omit } from 'lodash'

interface adsI {
  name: string
  quantity: number
}
interface subCampaignI {
  id?: string
  name: string
  status: boolean
  ads: adsI[]
}
interface initValueI {
  name: string
  describe: string
  subCampaigns: subCampaignI[]
}

const CampaignPage = () => {
  const init: initValueI = {
    name: '',
    describe: '',
    subCampaigns: [
      {
        id: new Date().getTime().toString(),
        name: `Chiến dịch con 1`,
        status: true,
        ads: [
          {
            name: 'Quảng cáo 1',
            quantity: 0
          }
        ]
      }
    ]
  }

  const [subCampaignActive, setSubCampaignActive] = useState(
    init.subCampaigns[0]
  )

  const [value, updateValue] = useReducer(
    (prev: any, curr: any) => ({ ...prev, ...curr }),
    init
  )

  const [errors, setErrors] = useState({})

  const idx = value.subCampaigns.findIndex(
    (e: subCampaignI) => e.id === subCampaignActive.id
  )

  const onSubmit = () => {
    validateSchema
      .validate(value, { abortEarly: false })
      .then((val: any) => {
        const formatData = {
          campaign: {
            information: {
              name: val.name,
              describe: val?.describe
            },
            subCampaigns: val.subCampaigns.map((e: subCampaignI) => ({
              name: e.name,
              status: e.status,
              ads: e.ads.map((a: adsI) => ({
                name: a.name,
                quantity: a.quantity
              }))
            }))
          }
        }
        alert(`Thêm thành công chiến dịch: ${JSON.stringify(formatData)}`)
      })
      .catch((err: { inner: any[] }) => {
        const validationErrors: any = {}
        err.inner.forEach((error) => {
          if (error.path) {
            validationErrors[error.path] = error.message
          }
        })
        setErrors(validationErrors)
      })
  }

  const getColor = (index: number): string => {
    const isValid = keys(errors).some((e) =>
      e.split('.').includes(`subCampaigns[${index}]`)
    )
    if (isValid) return 'red'
    return ''
  }

  const columns = [
    {
      field: 'name',
      headerName: 'Tên quảng cáo',
      align: 'left',
      width: 150,
      renderCell: ({ row }: any, index: number): React.JSX.Element => {
        return (
          <TextField
            id="adsName"
            variant="standard"
            error={!!get(errors, `subCampaigns[${idx}].ads[${index}].name`)}
            helperText={get(errors, `subCampaigns[${idx}].ads[${index}].name`)}
            onChange={(e) => {
              let subCampaigns = value.subCampaigns
              subCampaigns = [
                ...value.subCampaigns.slice(0, idx),
                {
                  ...value.subCampaigns[idx],
                  ads: [
                    ...value.subCampaigns[idx].ads.slice(0, index),
                    {
                      ...value.subCampaigns[idx].ads[index],
                      name: e.target.value
                    },
                    ...value.subCampaigns[idx].ads.slice(index + 1)
                  ]
                },
                ...value.subCampaigns.slice(idx + 1)
              ]
              updateValue({ ...value, subCampaigns: subCampaigns })
              setErrors(omit(errors, `subCampaigns[${idx}].ads[${index}].name`))
            }}
            value={row?.name}
            fullWidth
          />
        )
      }
    },
    {
      field: 'quantity',
      headerName: 'Số lượng',
      align: 'left',
      width: 100,
      renderCell: ({ row }: any, index: number) => {
        return (
          <TextField
            id="quantity"
            variant="standard"
            type="number"
            error={!!get(errors, `subCampaigns[${idx}].ads[${index}].quantity`)}
            helperText={get(
              errors,
              `subCampaigns[${idx}].ads[${index}].quantity`
            )}
            onChange={(e) => {
              let subCampaigns: Array<any> = value.subCampaigns
              subCampaigns = [
                ...value.subCampaigns.slice(0, idx),
                {
                  ...value.subCampaigns[idx],
                  ads: [
                    ...value.subCampaigns[idx].ads.slice(0, index),
                    {
                      ...value.subCampaigns[idx].ads[index],
                      quantity: e.target.value
                    },
                    ...value.subCampaigns[idx].ads.slice(index + 1)
                  ]
                },
                ...value.subCampaigns.slice(idx + 1)
              ]
              updateValue({ ...value, subCampaigns: subCampaigns })
              setErrors(
                omit(errors, `subCampaigns[${idx}].ads[${index}].quantity`)
              )
            }}
            value={row.quantity}
            fullWidth
          />
        )
      }
    },
    {
      field: 'action',
      headerName: '',
      width: 20,
      renderCell: (_: any, index: number) => {
        return (
          <IconButton
            onClick={() => {
              let subCampaigns = value.subCampaigns
              subCampaigns = [
                ...value.subCampaigns.slice(0, idx),
                {
                  ...value.subCampaigns[idx],
                  ads: [
                    ...value.subCampaigns[idx].ads.slice(0, index),
                    ...value.subCampaigns[idx].ads.slice(index + 1)
                  ]
                },
                ...value.subCampaigns.slice(idx + 1)
              ]
              updateValue({ ...value, subCampaigns: subCampaigns })
            }}
          >
            <Icon name="remove" />
          </IconButton>
        )
      }
    }
  ]

  return (
    <Page>
      <Grid
        container
        justifyContent={'center'}
        sx={{ p: 1, borderBottom: '1px solid gray' }}
        mt={1}
      >
        <Grid
          item
          xl={11}
          xs={12}
          sx={{ justifyContent: 'flex-end', display: 'flex' }}
        >
          <Button variant="contained" onClick={onSubmit}>
            submit
          </Button>
        </Grid>
      </Grid>
      <Grid container justifyContent={'center'}>
        <Grid item xl={11} xs={12}>
          <Paper elevation={1}>
            <Tabs list={['Thông tin', 'Chiến dịch con']} sx={{ mt: 1 }}>
              <Grid
                container
                rowSpacing={2}
                columnSpacing={{ xl: 8, xs: 4 }}
                sx={{ p: 2 }}
              >
                <Grid item xs={12}>
                  <TextField
                    id="name"
                    label="Tên chiến dịch"
                    variant="standard"
                    onChange={(e) => {
                      updateValue({ name: e.target.value })
                      setErrors(omit(errors, 'name'))
                    }}
                    value={value?.name}
                    error={errors.hasOwnProperty('name')}
                    helperText={get(errors, 'name')}
                    required
                    fullWidth
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="describe"
                    label="Mô tả"
                    variant="standard"
                    onChange={(e) => updateValue({ describe: e.target.value })}
                    value={value?.describe}
                    fullWidth
                  />
                </Grid>
              </Grid>
              <Grid
                container
                rowSpacing={2}
                columnSpacing={{ xl: 8, xs: 4 }}
                sx={{ p: 2 }}
              >
                <Grid item xs={12} overflow="auto">
                  <Box
                    display="flex"
                    flexDirection="row"
                    alignItems="center"
                    width={278 + 230 * (value.subCampaigns.length - 1)}
                  >
                    <IconButton
                      sx={{ width: 50, height: 50 }}
                      onClick={() =>
                        updateValue({
                          ...value,
                          subCampaigns: [
                            ...value.subCampaigns,
                            {
                              id: new Date().getTime().toString(),
                              name: `Chiến dịch con ${value.subCampaigns.length + 1}`,
                              status: true,
                              ads: [
                                {
                                  name: 'Quảng cáo 1',
                                  quantity: 0
                                }
                              ]
                            }
                          ]
                        })
                      }
                    >
                      <Icon name="add" fill="#0050AE" />
                    </IconButton>
                    {value?.subCampaigns?.map(
                      (sub: subCampaignI, index: number) => (
                        <Card
                          key={sub.id}
                          variant="outlined"
                          sx={{
                            width: 210,
                            height: 120,
                            cursor: 'pointer',
                            ...(sub.id === subCampaignActive.id
                              ? { border: '2px solid #0050AE' }
                              : {}),
                            ml: 2
                          }}
                          onClick={() => setSubCampaignActive(sub)}
                        >
                          <CardContent
                            sx={{
                              display: 'flex',
                              justifyContent: 'center',
                              flexWrap: 'wrap',
                              alignItems: 'center',
                              p: 1
                            }}
                          >
                            <Typography variant="h6" color={getColor(index)}>
                              {sub?.name}
                            </Typography>
                            <Icon
                              name="tick"
                              size={25}
                              {...(!!sub.status
                                ? {
                                    fill: 'rgb(0, 128, 0)'
                                  }
                                : {})}
                            />
                          </CardContent>
                          <CardContent
                            sx={{ display: 'flex', justifyContent: 'center' }}
                          >
                            <Typography variant="h6">
                              {sub.ads.reduce(
                                (
                                  prev: string | number,
                                  curr: { quantity: string | number }
                                ) => +prev + +curr.quantity,
                                0
                              )}
                            </Typography>
                          </CardContent>
                        </Card>
                      )
                    )}
                  </Box>
                </Grid>
                <Grid item xs={12} mt={1} display="flex">
                  <TextField
                    id="subCampaign"
                    label="Tên chiến dịch con"
                    variant="standard"
                    required
                    sx={{ flex: 2 }}
                    value={
                      value.subCampaigns.find(
                        (sub: subCampaignI) => sub.id === subCampaignActive.id
                      )?.name
                    }
                    error={!!get(errors, `subCampaigns[${idx}].name`)}
                    helperText={get(errors, `subCampaigns[${idx}].name`)}
                    onChange={(e) => {
                      let subCampaigns = value.subCampaigns
                      subCampaigns = [
                        ...value.subCampaigns.slice(0, idx),
                        {
                          ...value.subCampaigns[idx],
                          name: e.target.value
                        },
                        ...value.subCampaigns.slice(idx + 1)
                      ]
                      updateValue({ ...value, subCampaigns: subCampaigns })
                      setErrors(omit(errors, `subCampaigns[${idx}].name`))
                    }}
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={
                          value.subCampaigns.find(
                            (sub: subCampaignI) =>
                              sub.id === subCampaignActive.id
                          )?.status
                        }
                        onChange={(e) => {
                          let subCampaigns = value.subCampaigns
                          subCampaigns = [
                            ...value.subCampaigns.slice(0, idx),
                            {
                              ...value.subCampaigns[idx],
                              status: e.target.checked
                            },
                            ...value.subCampaigns.slice(idx + 1)
                          ]
                          updateValue({ ...value, subCampaigns: subCampaigns })
                        }}
                      />
                    }
                    label="Đang hoạt động"
                    sx={{ flex: 1, ml: 1 }}
                  />
                </Grid>
                <Grid item xs={12}>
                  <DataTable
                    columns={columns}
                    rows={
                      value.subCampaigns.find(
                        (e: subCampaignI) => e.id === subCampaignActive.id
                      ).ads || []
                    }
                    title="Danh sách quảng cáo"
                    beforeTopbar={
                      <Button
                        variant="outlined"
                        startIcon={<Icon name="add" fill={'#0050AE'} />}
                        onClick={() => {
                          let subCampaigns = value.subCampaigns
                          subCampaigns = [
                            ...value.subCampaigns.slice(0, idx),
                            {
                              ...value.subCampaigns[idx],
                              ads: [
                                ...value.subCampaigns[idx].ads,
                                {
                                  name: `Quảng cáo ${value.subCampaigns[idx].ads.length + 1}`,
                                  quantity: 0
                                }
                              ]
                            },
                            ...value.subCampaigns.slice(idx + 1)
                          ]
                          updateValue({ ...value, subCampaigns: subCampaigns })
                        }}
                      >
                        Thêm
                      </Button>
                    }
                  />
                </Grid>
              </Grid>
            </Tabs>
          </Paper>
        </Grid>
      </Grid>
    </Page>
  )
}

export default CampaignPage
