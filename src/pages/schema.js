import * as Yup from 'yup'

export const validateSchema = Yup.object().shape({
  name: Yup.string().required('Mục này bắt buộc phải nhập'),
  describe: Yup.string(),
  subCampaigns: Yup.array().of(
    Yup.object().shape({
      name: Yup.string().required('Mục này bắt buộc phải nhập'),
      status: Yup.bool(),
      ads: Yup.array().of(
        Yup.object().shape({
          name: Yup.string().required('Mục này bắt buộc phải nhập'),
          quantity: Yup.number()
            .nullable()
            .required('Mục này bắt buộc phải nhập')
            .min(1, 'Giá trị phải lớn hơn 0')
        })
      )
    })
  )
})
