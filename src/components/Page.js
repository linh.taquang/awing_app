import { Box } from '@mui/material'

const Page = ({ children }) => {
  return (
    <Box
      sx={{
        boxSizing: 'border-box',
        height: '100%'
      }}
    >
      {children}
    </Box>
  )
}

export default Page
