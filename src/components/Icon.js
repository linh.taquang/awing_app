import { Box } from '@mui/system'

import { ReactComponent as Add } from 'src/assets/images/add.svg'
import { ReactComponent as Tick } from 'src/assets/images/tick.svg'
import { ReactComponent as Remove } from 'src/assets/images/remove.svg'

export const icons = {
  add: <Add />,
  tick: <Tick />,
  remove: <Remove />
}

const Icon = ({ name = '', fill = '', size = 20 }) => {
  return (
    <Box
      component="span"
      className="x-icon"
      sx={{
        display: 'inline-flex',
        alignItems: 'center',
        justifyContent: 'center',
        ...(size
          ? {
              width: size,
              height: size
            }
          : {}),
        svg: {
          width: '100%',
          height: '100%',
          path: { fill: fill }
        }
      }}
    >
      {icons[name]}
    </Box>
  )
}

export default Icon
