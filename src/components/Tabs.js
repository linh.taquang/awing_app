import { Box, Tab } from '@mui/material'
import MuiTabs from '@mui/material/Tabs'
import { isArray } from 'lodash'
import { useState } from 'react'

const Tabs = ({ list = [], onChange = () => {}, children, sx = {} }) => {
  const [value, setValue] = useState(0)

  const handleChange = (_, newValue) => {
    setValue(newValue)
    onChange(newValue)
  }

  const renderContent = () => {
    if (isArray(children)) return children?.[value] ?? null

    return children
  }

  return (
    <>
      <Box sx={{ borderBottom: 1, borderColor: 'divider', ...sx }}>
        <MuiTabs value={value ?? 0} onChange={handleChange}>
          {list.map((item, index) => {
            return <Tab key={`Tab-${index}`} label={item} wrapped />
          })}
        </MuiTabs>
      </Box>
      {renderContent()}
    </>
  )
}

export default Tabs
